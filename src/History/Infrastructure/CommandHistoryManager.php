<?php

namespace Jakmall\Recruitment\Calculator\History\Infrastructure;

class CommandHistoryManager implements CommandHistoryManagerInterface
{
    protected $filePath = 'storage/mesinhitung.log';

    protected $latestPath = 'storage/latest.log';

    public function findAll($driver): array
    {
        /**
         * Validate selected driver
         */
        if ($driver == 'file') {
            /**
             * Get all file data
             */
            $data = $this->retrieveAllFileData();
        } elseif ($driver == 'latest') {
            /**
             * Get all latest data
             */
            $data = $this->retrieveAllLatestData();
        } else {
            /**
             * Get all file data if selectedDriver is composite
             */
            $data = $this->retrieveAllFileData();
        }

        return $data;
    }

    public function find($id, $driver)
    {
        /**
         * Validate selected driver
         */
        if ($driver == 'file') {
            /**
             * Find file data
             */
            $result = $this->findFileData($id);
        } elseif ($driver == 'latest') {
            /**
             * Find latest data
             */
            $result = $this->findLatestData($id);
        } else {
            /**
             * When composite chosen, use latest driver first
             */
            $result = $this->findLatestData($id);

            /**
             * If not found, then find in file driver
             */
            if (empty($result)) {
                $result = $this->findFileData($id);
            }
        }

        return $result;
    }

    public function log($command): bool
    {
        /**
         * Store to mesinghitung.log
         */
        file_put_contents($this->filePath, $command . ";\n", FILE_APPEND);

        /**
         * Store to latest.log
         */
        $lastState = ($this->lastState()) + 1;
        $decodedCommand = json_decode($command, true);

        /**
         * Validate latest.log existance
         */
        if (file_exists($this->latestPath)) {
            $existingLatestLog = file_get_contents($this->latestPath);
            $existingLatestData = explode(";\n", substr($existingLatestLog, 0, -2));

            /**
             * Validate amount of latest data
             */
            if (count($existingLatestData) < 10) {
                $newLatestData = array(
                    "id" => $decodedCommand['id'],
                    "command" => $decodedCommand['command'],
                    "operation" => $decodedCommand['operation'],
                    "result" => $decodedCommand['result'],
                    "state" => $lastState
                );

                file_put_contents($this->latestPath, json_encode($newLatestData) . ";\n", FILE_APPEND);

            } else {
                /**
                 * Decode every single json data
                 */
                $modifiedExistingLatestData = array();
                foreach ($existingLatestData as $index => $row) {
                    $modifiedExistingLatestData[$index] = json_decode($row, true);
                }

                /**
                 * Sort the data by asc order
                 */
                usort($modifiedExistingLatestData, function(array $a, array $b) {
                    return $a['stated'] - $b['stated'];
                });

                /**
                 * Remove a data with the smallest state value
                 */
                unset($modifiedExistingLatestData[0]);

                /**
                 * Append new latest data
                 */
                $newLatestData = array(
                    "id" => $decodedCommand['id'],
                    "command" => $decodedCommand['command'],
                    "operation" => $decodedCommand['operation'],
                    "result" => $decodedCommand['result'],
                    "state" => $lastState
                );
                array_push($modifiedExistingLatestData, $newLatestData);

                /**
                 * Re-ordering state value
                 */
                $newestLatestData = '';
                foreach ($modifiedExistingLatestData as $k => $r) {
                    $newestLatestData .= json_encode(
                        array(
                            "id" => $r['id'],
                            "command" => $r['command'],
                            "operation" => $r['operation'],
                            "result" => $r['result'],
                            "state" => $k
                        )
                    ) . ";\n";
                }

                /**
                 * Store data
                 */
                file_put_contents($this->latestPath, $newestLatestData);
            }

        } else {
            $newLatestData = array(
                "id" => $decodedCommand['id'],
                "command" => $decodedCommand['command'],
                "operation" => $decodedCommand['operation'],
                "result" => $decodedCommand['result'],
                "state" => $lastState
            );

            file_put_contents($this->latestPath, json_encode($newLatestData) . ";\n", FILE_APPEND);
        }

        return true;
    }

    public function clear($id): bool
    {
        /**
         * Delete file data
         */
        $fileDeleted = $this->deleteFileById($id);

        /**
         * Delete latest data
         */
        $latestDeleted = $this->deleteLatestById($id);

        return true;
    }

    public function clearAll(): bool
    {
        /**
         * Delete all file data
         */
        $fileDeleted = $this->deleteAllFile();

        /**
         * Delete all latest data
         */
        $latestDeleted = $this->deleteAllLatest();

        return true;
    }

    public function retrieveAllFileData(): array
    {
        $data = array();

        /**
         * Validate log file and retrieve log data
         */
        $fileData = $this->validateAndRetrieveFile($this->filePath);

        foreach ($fileData as $row) {
            $data[] = json_decode($row, true);
        }

        return $data;
    }

    public function retrieveAllLatestData(): array
    {
        $data = array();

        /**
         * Validate log file and retrieve log data
         */
        $latestData = $this->validateAndRetrieveFile($this->latestPath);

        /**
         * Decoding json format
         */
        foreach ($latestData as $key => $row) {
            $temp = json_decode($row, true);
            $latestData[$key] = $temp;
        }

        /**
         * Sort data by desc order of state field
         */
        usort($latestData, function(array $a, array $b) {
            return $b['state'] - $a['state'];
        });

        /**
         * Remove state field after the data sorted by its state
         */
        foreach ($latestData as $latest) {
            $data[] = array(
                'id' => $latest['id'],
                'command' => $latest['command'],
                'operation' => $latest['operation'],
                'result' => $latest['result'],
            );
        }

        return $data;
    }

    public function findFileData(int $id): array
    {
        $result = array();

        /**
         * Get all file data
         */
        $data = $this->retrieveAllFileData();

        if (count($data) < 1) {
            return $result;
        }

        foreach ($data as $key => $row) {
            if ($row['id'] == $id) {
                $result[] = array(
                    'id' => $row['id'],
                    'command' => $row['command'],
                    'operation' => $row['operation'],
                    'result' => $row['result']
                );
            }
        }

        return $result;
    }

    public function findLatestData(int $id): array
    {
        $result = array();

        /**
         * Get all latest data
         */
        $data = $this->retrieveAllLatestData();

        if (count($data) < 1) {
            return $result;
        }

        foreach ($data as $key => $row) {
            if ($row['id'] == $id) {
                $result[] = array(
                    'id' => $row['id'],
                    'command' => $row['command'],
                    'operation' => $row['operation'],
                    'result' => $row['result']
                );
            }
        }

        /**
         * Update state value of the selected data before if data found
         */
        if (!empty($result)) {
            $this->updateStateLatestData($result);
        }

        return $result;
    }

    public function updateStateLatestData($selectedData)
    {
        /**
         * Validate log file and retrieve log data
         */
        $latestData = $this->validateAndRetrieveFile($this->latestPath);

        /**
         * Decode every single json data
         */
        $modifiedLatestData = array();
        foreach ($latestData as $index => $row) {
            $modifiedLatestData[$index] = json_decode($row, true);
        }

        /**
         * Sort data by desc order
         */
        usort($modifiedLatestData, function (array $a, array $b) {
            return $a['state'] - $b['state'];
        });

        /**
         * Get the last state
         */
        $lastState = $this->lastState();

        /**
         * Get row of selected data
         */
        $selectedData[0]['state'] = $lastState;

        /**
         * Assign a new array of latest data with new sequence of state
         */
        $updatedLatestData = array();
        $i = 1;
        foreach ($modifiedLatestData as $key => $row) {
            if ($row['id'] != $selectedData[0]['id']) {
                $updatedLatestData[] = array(
                    'id' => $row['id'],
                    'command' => $row['command'],
                    'operation' => $row['operation'],
                    'result' => $row['result'],
                    'state' => $i
                );

                $i++;
            }
        }

        array_push($updatedLatestData, $selectedData[0]);

        /**
         * Decode every single row to json format
         */
        $decodedLatestData = '';
        foreach ($updatedLatestData as $key => $row) {
            $decodedLatestData .= json_encode(
                array(
                    "id" => $row['id'],
                    "command" => $row['command'],
                    "operation" => $row['operation'],
                    "result" => $row['result'],
                    "state" => $row['state']
                )
            ) . ";\n";
        }

        /**
         * Store data
         */
        file_put_contents($this->latestPath, $decodedLatestData);
    }

    public function deleteFileById(int $id)
    {
        /**
         * Validate if the specified data is exist
         */
        $selectedData = $this->findFileData($id);

        if (empty($selectedData)) {
            return false;
        }

        /**
         * Get all file data
         */
        $sourceData = $this->retrieveAllFileData();

        /**
         * Unset selected data from source
         */
        foreach ($sourceData as $i => $v) {
            if ($v['id'] == $selectedData[0]['id']) {
                unset($sourceData[$i]);
            }
        }

        /**
         * Decode new file data
         */
        $decodedFileData = '';
        foreach ($sourceData as $i => $v) {
            $decodedFileData .= json_encode($v) . ";\n";
        }

        /**
         * Store to mesinhitung.log
         */
        file_put_contents($this->filePath, $decodedFileData);

        return true;
    }

    public function deleteLatestById($id)
    {
        /**
         * Validate if the specified data is exist
         */
        $selectedData =$this->findLatestData($id);

        if (empty($selectedData)) {
            return false;
        }

        /**
         * Get all latest data
         */
        $sourceData = $this->retrieveAllLatestData();

        /**
         * Unset selected data from source
         */
        foreach ($sourceData as $i => $v) {
            if ($v['id'] == $selectedData[0]['id']) {
                unset($sourceData[$i]);
            }
        }

        /**
         * Assign new order of state value
         * Encode each row into json format
         */
        $startState = count($sourceData);
        $encodedData = '';

        foreach ($sourceData as $i => $v) {
            $sourceData[$i]['state'] = $v['state'] = $startState;
            $startState -= 1;

            $encodedData .= json_encode($v) . ";\n";
        }

        /**
         * Store to latest.log
         */
        file_put_contents($this->latestPath, $encodedData);

        return true;
    }

    public function deleteAllFile()
    {
        file_put_contents($this->filePath, "");
    }

    public function deleteAllLatest()
    {
        file_put_contents($this->latestPath, "");
    }

    public function validateAndRetrieveFile($path): array
    {
        /**
         * Initialize result variable
         */
        $data = array();

        /**
         * Validate log existance
         */
        if (!file_exists($path)) {
            return $data;
        }

        $logContents = file_get_contents($path);
        $logData = explode(";\n", substr($logContents, 0, -2));

        /**
         * Return empty array if latest.log file is empty
         */
        if (empty($logData)) {
            return $data;
        }

        return $logData;
    }

    public function lastId(): int
    {
        /**
         * Validate file existance
         */
        if (!file_exists($this->filePath)) {
            return 0;
        }

        $file = file_get_contents($this->filePath);
        $rows = explode("\n", substr($file, 0, -2));

        /**
         * Return 0 if file is empty
         */
        if (empty($rows)) {
            return 0;
        }

        /**
         * Get the last row
         */
        $endOfRow = end($rows);

        $lastId = json_decode($endOfRow, true) ?? 0;

        return $lastId['id'] ?? $lastId;
    }

    public function lastState(): int
    {
        /**
         * Validate file existance
         */
        if (!file_exists($this->latestPath)) {
            return 0;
        }

        $latest = file_get_contents($this->latestPath);
        $rows = explode("\n", substr($latest, 0, -2));

        /**
         * Return 0 if file is empty
         */
        if (empty($rows)) {
            return 0;
        }

        /**
         * Get the last row
         */
        $endOfRow = end($rows);

        $lastState = json_decode($endOfRow, true) ?? 0;

        return $lastState['state'] ?? $lastState;
    }
}
