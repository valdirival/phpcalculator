<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class CoreCommand extends Command
{
    /**
     * @var string
     */
    protected $signature;

    /**
     * @var string
     */
    protected $description;

    protected $history;

    public function __construct(CommandHistoryManagerInterface $history)
    {
        $this->history = $history;

        $this->signature = sprintf(
            '%s {numbers* : The numbers to be %s}',
            $this->commandVerb,
            $this->commandPassiveVerb
        );
        $this->description = sprintf('%s all given Numbers', ucfirst($this->commandVerb));

        parent::__construct();
    }

    public function handle(): void
    {
        $numbers = $this->getInput();
        $description = $this->generateCalculationDescription($numbers);
        $result = $this->calculateAll($numbers);

        /**
         * Get last id of the record file
         */
        $lastId = $this->history->lastId();
        $currentId = $lastId + 1;

        $data = json_encode(
            array(
                "id" => $currentId,
                "command" => $this->commandVerb,
                "operation" => $description,
                "result" => $result
            )
        );

        $logged = $this->history->log($data);

        $this->comment(sprintf('%s = %s', $description, $result));
    }

    protected function getInput(): array
    {
        return $this->argument('numbers');
    }

    protected function generateCalculationDescription(array $numbers): string
    {
        $operator = $this->operator;
        $glue = sprintf(' %s ', $operator);

        return implode($glue, $numbers);
    }

    /**
     * @param array $numbers
     *
     * @return float|int
     */
    protected function calculateAll(array $numbers)
    {
        $number = array_pop($numbers);

        if (count($numbers) <= 0) {
            return $number;
        }

        return $this->calculate($this->calculateAll($numbers), $number);
    }
}
