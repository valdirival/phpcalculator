<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Jakmall\Recruitment\Calculator\Commands\CoreCommand as CoreCommand;

class AddCommand extends CoreCommand
{
    /**
     * @var string
     */
    protected $commandVerb = 'add';

    /**
     * @var string
     */
    protected $commandPassiveVerb = 'added';

    /**
     * @var string
     */
    protected $operator = '+';

    /**
     * @param int|float $number1
     * @param int|float $number2
     *
     * @return int|float
     */
    protected function calculate($number1, $number2)
    {
        return $number1 + $number2;
    }
}
