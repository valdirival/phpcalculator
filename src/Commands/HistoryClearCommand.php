<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class HistoryClearCommand extends Command
{
    /**
     * @var string
     */
    protected $signature;

    /**
     * @var string
     */
    protected $commandVerb = 'history:clear';

    /**
     * @var string
     */
    protected $option = 'composite';

    protected $history;

    public function __construct(CommandHistoryManagerInterface $history)
    {
        $this->history = $history;

        $this->signature = sprintf(
            '%s {id? : Specified id to delete data by its id}',
            $this->commandVerb
        );

        $this->description = "Clear all history data or by the id if it specified";

        parent::__construct();
    }

    public function handle(): void
    {
        /**
         * Retrieve user input
         */
        $id = $this->argument('id');

        if ($id) {
            $clearSucceed = $this->history->clear($id);
            $descComment = sprintf('Data with ID %s is removed', $id);

            $this->comment($descComment);
        } else {
            $clearSucceed = $this->history->clearAll();

            $this->comment('All history is cleared');
        }
    }
}
