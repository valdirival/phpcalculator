<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Jakmall\Recruitment\Calculator\Commands\CoreCommand as CoreCommand;

class DivideCommand extends CoreCommand
{
    /**
     * @var string
     */
    protected $commandVerb = 'divide';

    /**
     * @var string
     */
    protected $commandPassiveVerb = 'divided';

    /**
     * @var string
     */
    protected $operator = '/';

    /**
     * @param int|float $number1
     * @param int|float $number2
     *
     * @return int|float
     */
    protected function calculate($number1, $number2)
    {
        return $number1 / $number2;
    }
}
