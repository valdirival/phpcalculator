<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class HistoryListCommand extends Command
{
    /**
     * @var string
     */
    protected $signature;

    /**
     * @var string
     */
    protected $commandVerb = 'history:list';

    /**
     * @var string
     */
    protected $option = 'composite';

    protected $history;

    public function __construct(CommandHistoryManagerInterface $history)
    {
        $this->history = $history;

        $this->signature = sprintf(
            '%s {id? : Specified id to display the data} {--D|--driver=composite : Option driver [file|latest|composite]}',
            $this->commandVerb
        );

        $this->description = "List of executed calculation commands";

        parent::__construct();
    }

    public function handle(): void
    {
        /**
         * Retrieve user input
         */
        $id = $this->argument('id');
        $selectedDriver = $this->option('driver');

        /**
         * Validate action by id value
         */
        if ($id) {
            $historyData = $this->history->find($id, $selectedDriver);
        } else {
            $historyData = $this->history->findAll($selectedDriver);
        }

        /**
         * Show data in table format
         */
        $header = ['ID', 'Command', 'Operation', 'Result'];

        if (count($historyData) > 0) {
            $this->table($header, $historyData);
        } else {
            $this->info('History not found or empty');
        }
    }
}
