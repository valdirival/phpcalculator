<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Jakmall\Recruitment\Calculator\Commands\CoreCommand as CoreCommand;

class SubtractCommand extends CoreCommand
{
    /**
     * @var string
     */
    protected $commandVerb = 'subtract';

    /**
     * @var string
     */
    protected $commandPassiveVerb = 'subtracted';

    /**
     * @var string
     */
    protected $operator = '-';

    /**
     * @param int|float $number1
     * @param int|float $number2
     *
     * @return int|float
     */
    protected function calculate($number1, $number2)
    {
        return $number1 - $number2;
    }
}
