<?php

namespace Jakmall\Recruitment\Calculator\Http\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class HistoryController
{
    protected $history;

    public function __construct(CommandHistoryManagerInterface $history)
    {
        $this->history = $history;
    }
    public function index(Request $request)
    {
        $historyData = $this->history->findAll($request->driver);

        return JsonResponse::create(
            $historyData,
            200
        );
    }

    public function show(Request $request, $id)
    {
        $historyData = $this->history->find($id, $request->driver);

        if ($historyData[0]['command'] == 'add') {
            $input = explode('+', $historyData[0]['operation']);
        } elseif ($historyData[0]['command'] == 'subtract') {
            $input = explode('-', $historyData[0]['operation']);
        } elseif ($historyData[0]['command'] == 'multiply') {
            $input = explode('*', $historyData[0]['operation']);
        } elseif ($historyData[0]['command'] == 'divide') {
            $input = explode('/', $historyData[0]['operation']);
        } else {
            $input = explode('^', $historyData[0]['operation']);
        }

        $response = array(
            "id" => $historyData[0]['id'],
            "command" => $historyData[0]['command'],
            "operation" => $historyData[0]['operation'],
            "input" => $input,
            "result" => $historyData[0]['result']
        );

        return JsonResponse::create(
            $response,
            200
        );
    }

    public function remove($id)
    {
        $deleteHistory = $this->history->clear($id);

        return JsonResponse::create(null, 204);
    }
}
