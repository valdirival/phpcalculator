<?php

namespace Jakmall\Recruitment\Calculator\Http\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class CalculatorController
{
    protected $history;
    protected $availableCommand = ['add', 'subtract', 'multiply', 'divide', 'power'];

    public function __construct(CommandHistoryManagerInterface $history)
    {
        $this->history = $history;
    }

    public function calculate(Request $request, $userCommand)
    {
        /**
         * Validate user command input
         */
        if (!in_array($userCommand, $this->availableCommand)) {
            return JsonResponse::create(
                ['message' => 'Command invalid'],
                404
            );
        }

        /**
         * Validate user number input
         */
        if (!is_array($request->input('input'))) {
            return JsonResponse::create(
                ['message' => 'Input must an array format'],
                404
            );
        }

        /**
         * Calculate process
         */
        $result = $this->calculateAll($request->input('input'), $userCommand);

        /**
         * Get operation description
         */
        $operation = $this-> generateCalculationDescription($request->input('input'), $userCommand);

        /**
         * Get last id of the record file
         */
        $lastId = $this->history->lastId();
        $currentId = $lastId + 1;

        $data = json_encode(
            array(
                "id" => $currentId,
                "command" => $userCommand,
                "operation" => $operation,
                "result" => $result
            )
        );

        /**
         * Log to history
         */
        $logged = $this->history->log($data);

        return JsonResponse::create([
            'command' => $userCommand,
            'operation' => $operation,
            'result' => $result
        ], 200);
    }

    public function calculateAll(array $numbers, $command)
    {
        $number = array_pop($numbers);

        if (count($numbers) <= 0) {
            return $number;
        }

        return $this->calculateNumber($this->calculateAll($numbers, $command), $number, $command);
    }

    public function calculateNumber($number1, $number2, $command)
    {
        if ($command == 'add') {
            return $number1 + $number2;
        } elseif ($command == 'subtract') {
            return $number1 - $number2;
        } elseif ($command == 'multiply') {
            return $number1 * $number2;
        } elseif ($command == 'divide') {
            return $number1 / $number2;
        } else {
            return $number1 ** $number2;
        }
    }

    public function generateOperation($command): string
    {
        if ($command == 'add') {
            return '+';
        } elseif ($command == 'subtract') {
            return '-';
        } elseif ($command == 'multiply') {
            return '*';
        } elseif ($command == 'divide') {
            return '/';
        } else {
            return '^';
        }
    }

    protected function generateCalculationDescription(array $numbers, $command): string
    {
        $operator = $this->generateOperation($command);
        $glue = sprintf(' %s ', $operator);

        return implode($glue, $numbers);
    }
}
